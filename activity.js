/*3. The questions are as follows:
- What directive is used by Node.js in loading the modules it needs?
		"Require" directive

- What Node.js module contains a method for server creation?
		Hyper Text Transfer Protocol/ HTTP

- What is the method of the http object responsible for creating a server using Node.js?
		createServer()

- What method of the response object allows us to set status codes and content types?
		writeHead

- Where will console.log() output its contents when run in Node.js?
		Terminal

- What property of the request object contains the address's endpoint?
		Port
*/